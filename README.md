<h1>Scrambler</h1>
<div><b>Free, Open-Source productivity negation program coded in Golang</b></div>
<div>This annoying little program is guaranteed to diminish both the sanity and 
productivity of your victim(s) at the same time!</div>
<h2>Disclaimer</h2>
<div>This code is provided for educational purposes only.</div>
<div>I am not responsible for any damage(s) caused by any use of this code.</div>
<div>Please use with caution.</div>

<h2>Features</h2>
<h3>This program periodically...</h3>
- Types random strings of up to 100 characters!
- Clicks/Double clicks randomly!
- Scrolls up/down randomly!
- Moves the cursor to a random location on the screen!

<h3>It is also...</h3>

- Fully customizable, edit the code to your liking!

<h2>Requirements</h2>
<h3>Golang Version 1.10</h3>
<div>https://golang.org</div>
<h3>RobotGo Package</h3>
<div>https://github.com/go-vgo/robotgo</div>
<h3>GCC</h3>
<div>https://gcc.gnu.org</div>
<div>Note: If you are using a 64-bit version of Golang, you will need to install a 64-bit version of GCC such as MinGW-w64 as well.</div>
<div>This same logic also applies to 32-bit versions of Golang.</div>

<h2>Build</h2>
<h3>Windows</h3>
Note: The flags '-ldflags' and '-H=windowsgui' are used to hide the console of the program
```
go build -ldflags -H=windowsgui -o scrambler.exe scrambler.go
```

<h3>Other Platforms</h3>
```
go build -o scrambler.exe scrambler.go
```

<h2>Credits</h2>
<div><b>RobotGo</b></div>
<div>Unknown Copyright Year</div>
<div>Unknown Copyright Owner</div>
<div>Apache 2.0 License</div>
<div>MIT License</div>
<div>https://github.com/go-vgo/robotgo</div>